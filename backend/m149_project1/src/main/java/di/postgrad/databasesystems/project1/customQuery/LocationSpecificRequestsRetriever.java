package di.postgrad.databasesystems.project1.customQuery;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

public class LocationSpecificRequestsRetriever {

	private Session activeSession;
	private int verboseLevel;
	private QueryLogger queryLogger;

	public LocationSpecificRequestsRetriever(Session _activeSession, int _verboseLevel) {
		activeSession = _activeSession;
		verboseLevel = _verboseLevel;
		queryLogger = new QueryLogger(_activeSession, _verboseLevel);
	}
	
	 @SuppressWarnings("unchecked")
	    public Iterable<BigInteger> findLocationSpecificAbandonedVehicles(
	            String username,
	    		String street_address,
	            Integer zip_code
	            ) {
		 	
		 String sqlLog = null;
		 	if (zip_code != null) {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, abandoned_vehicles as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND ((l.zip_code = " + zip_code + " ) OR ( " + zip_code + " is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
		 	else {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, abandoned_vehicles as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
	        
	        
	        queryLogger.logQueryForUser(username, sqlLog);

	        activeSession.beginTransaction();
	        
	        String sql = null;
	        if (zip_code != null) {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, abandoned_vehicles as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND ((l.zip_code = :zip_code) OR (:zip_code is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";	
	        }
	        else {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, abandoned_vehicles as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";
	        }

	        
	        if (verboseLevel>0) {
	            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
	        }

	        Iterable<BigInteger> result = null;
	        try {

	        	System.out.println(username);

	        	Query query = activeSession
	        			.createSQLQuery(sql);

	        	sql = query.getQueryString();
	        	if (zip_code != null) {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
							.setParameter("zip_code", zip_code)
                            .list();
	        	}
	        	else {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
                            .list();
	        	}
	        	

	        	if (verboseLevel>0) {
					System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
				}
	        	if (verboseLevel>1) {
	        		System.out.println("[CustomQuery] Output:");
	        		for(BigInteger r : result){
	       				System.out.print("\t");
	        			System.out.print(r);
	       				System.out.println("\t");
	        		}
	        	}
	        }
	        catch(Exception e) {
	        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
	        }

	        activeSession.getTransaction().commit();
	        return result;
	        }
	 
	 @SuppressWarnings("unchecked")
	    public Iterable<BigInteger> findLocationSpecificGarbageCarts(
	    		String username,
	    		String street_address,
	            Integer zip_code
	            ) {
		 	
		 String sqlLog = null;
		 	if (zip_code != null) {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, garbage_carts as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND ((l.zip_code = " + zip_code + " ) OR ( " + zip_code + " is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
		 	else {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, garbage_carts as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
	        
	        
	        queryLogger.logQueryForUser(username, sqlLog);

	        activeSession.beginTransaction();
	        
	        String sql = null;
	        if (zip_code != null) {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, garbage_carts as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND ((l.zip_code = :zip_code) OR (:zip_code is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";	
	        }
	        else {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, garbage_carts as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";
	        }

	        
	        if (verboseLevel>0) {
	            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
	        }

	        Iterable<BigInteger> result = null;
	        try {

	        	System.out.println(username);

	        	Query query = activeSession
	        			.createSQLQuery(sql);

	        	sql = query.getQueryString();
	        	if (zip_code != null) {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
							.setParameter("zip_code", zip_code)
                            .list();
	        	}
	        	else {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
                            .list();
	        	}
	        	

	        	if (verboseLevel>0) {
					System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
				}
	        	if (verboseLevel>1) {
	        		System.out.println("[CustomQuery] Output:");
	        		for(BigInteger r : result){
	       				System.out.print("\t");
	        			System.out.print(r);
	       				System.out.println("\t");
	        		}
	        	}
	        }
	        catch(Exception e) {
	        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
	        }

	        activeSession.getTransaction().commit();
	        return result;
	            }
	 
	 @SuppressWarnings("unchecked")
	    public Iterable<BigInteger> findLocationSpecificGraffitiRemoval(
	    		String username,
	    		String street_address,
	            Integer zip_code
	            ) {
		 	
		 String sqlLog = null;
		 	if (zip_code != null) {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, graffiti_removal as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND ((l.zip_code = " + zip_code + " ) OR ( " + zip_code + " is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
		 	else {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, graffiti_removal as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
	        
	        
	        queryLogger.logQueryForUser(username, sqlLog);

	        activeSession.beginTransaction();
	        
	        String sql = null;
	        if (zip_code != null) {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, graffiti_removal as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND ((l.zip_code = :zip_code) OR (:zip_code is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";	
	        }
	        else {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, graffiti_removal as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";
	        }

	        
	        if (verboseLevel>0) {
	            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
	        }

	        Iterable<BigInteger> result = null;
	        try {

	        	System.out.println(username);

	        	Query query = activeSession
	        			.createSQLQuery(sql);

	        	sql = query.getQueryString();
	        	if (zip_code != null) {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
							.setParameter("zip_code", zip_code)
                            .list();
	        	}
	        	else {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
                            .list();
	        	}
	        	

	        	if (verboseLevel>0) {
					System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
				}
	        	if (verboseLevel>1) {
	        		System.out.println("[CustomQuery] Output:");
	        		for(BigInteger r : result){
	       				System.out.print("\t");
	        			System.out.print(r);
	       				System.out.println("\t");
	        		}
	        	}
	        }
	        catch(Exception e) {
	        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
	        }

	        activeSession.getTransaction().commit();
	        return result;
	            }
	 
	 @SuppressWarnings("unchecked")
	    public Iterable<BigInteger> findLocationSpecificPotHolesReported(
	    		String username,
	    		String street_address,
	            Integer zip_code
	            ) {
		 	
		 String sqlLog = null;
		 	if (zip_code != null) {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, pot_holes_reported as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND ((l.zip_code = " + zip_code + " ) OR ( " + zip_code + " is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
		 	else {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, pot_holes_reported as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
	        
	        
	        queryLogger.logQueryForUser(username, sqlLog);

	        activeSession.beginTransaction();
	        
	        String sql = null;
	        if (zip_code != null) {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, pot_holes_reported as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND ((l.zip_code = :zip_code) OR (:zip_code is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";	
	        }
	        else {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, pot_holes_reported as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";
	        }

	        
	        if (verboseLevel>0) {
	            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
	        }

	        Iterable<BigInteger> result = null;
	        try {

	        	System.out.println(username);

	        	Query query = activeSession
	        			.createSQLQuery(sql);

	        	sql = query.getQueryString();
	        	if (zip_code != null) {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
							.setParameter("zip_code", zip_code)
                            .list();
	        	}
	        	else {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
                            .list();
	        	}
	        	

	        	if (verboseLevel>0) {
					System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
				}
	        	if (verboseLevel>1) {
	        		System.out.println("[CustomQuery] Output:");
	        		for(BigInteger r : result){
	       				System.out.print("\t");
	        			System.out.print(r);
	       				System.out.println("\t");
	        		}
	        	}
	        }
	        catch(Exception e) {
	        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
	        }

	        activeSession.getTransaction().commit();
	        return result;
	            }
	 
	 @SuppressWarnings("unchecked")
	    public Iterable<BigInteger> findLocationSpecificRodentBaiting(
	            String username,
	            String street_address,
	            Integer zip_code
	            ) {
		 	
		 String sqlLog = null;
		 	if (zip_code != null) {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, rodent_baiting as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND ((l.zip_code = " + zip_code + " ) OR ( " + zip_code + " is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
		 	else {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, rodent_baiting as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
	        
	        
	        queryLogger.logQueryForUser(username, sqlLog);

	        activeSession.beginTransaction();
	        
	        String sql = null;
	        if (zip_code != null) {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, rodent_baiting as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND ((l.zip_code = :zip_code) OR (:zip_code is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";	
	        }
	        else {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, rodent_baiting as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";
	        }

	        
	        if (verboseLevel>0) {
	            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
	        }

	        Iterable<BigInteger> result = null;
	        try {

	        	System.out.println(username);

	        	Query query = activeSession
	        			.createSQLQuery(sql);

	        	sql = query.getQueryString();
	        	if (zip_code != null) {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
							.setParameter("zip_code", zip_code)
                            .list();
	        	}
	        	else {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
                            .list();
	        	}
	        	

	        	if (verboseLevel>0) {
					System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
				}
	        	if (verboseLevel>1) {
	        		System.out.println("[CustomQuery] Output:");
	        		for(BigInteger r : result){
	       				System.out.print("\t");
	        			System.out.print(r);
	       				System.out.println("\t");
	        		}
	        	}
	        }
	        catch(Exception e) {
	        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
	        }

	        activeSession.getTransaction().commit();
	        return result;
	            }
	 
	 @SuppressWarnings("unchecked")
	    public Iterable<BigInteger> findLocationSpecificSanitationCodeComplaints(
	    		String username,
	    		String street_address,
	            Integer zip_code
	            ) {
		 	
		 String sqlLog = null;
		 	if (zip_code != null) {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, sanitation_code_complaints as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND ((l.zip_code = " + zip_code + " ) OR ( " + zip_code + " is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
		 	else {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, sanitation_code_complaints as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
	        
	        
	        queryLogger.logQueryForUser(username, sqlLog);

	        activeSession.beginTransaction();
	        
	        String sql = null;
	        if (zip_code != null) {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, sanitation_code_complaints as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND ((l.zip_code = :zip_code) OR (:zip_code is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";	
	        }
	        else {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, sanitation_code_complaints as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";
	        }

	        
	        if (verboseLevel>0) {
	            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
	        }

	        Iterable<BigInteger> result = null;
	        try {

	        	System.out.println(username);

	        	Query query = activeSession
	        			.createSQLQuery(sql);

	        	sql = query.getQueryString();
	        	if (zip_code != null) {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
							.setParameter("zip_code", zip_code)
                            .list();
	        	}
	        	else {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
                            .list();
	        	}
	        	

	        	if (verboseLevel>0) {
					System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
				}
	        	if (verboseLevel>1) {
	        		System.out.println("[CustomQuery] Output:");
	        		for(BigInteger r : result){
	       				System.out.print("\t");
	        			System.out.print(r);
	       				System.out.println("\t");
	        		}
	        	}
	        }
	        catch(Exception e) {
	        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
	        }

	        activeSession.getTransaction().commit();
	        return result;
	            }
	 
	 @SuppressWarnings("unchecked")
	    public Iterable<BigInteger> findLocationSpecificTreeDebris(
	    		String username,
	    		String street_address,
	            Integer zip_code
	            ) {
		 	
		 String sqlLog = null;
		 	if (zip_code != null) {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, tree_debris as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND ((l.zip_code = " + zip_code + " ) OR ( " + zip_code + " is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
		 	else {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, tree_debris as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
	        
	        
	        queryLogger.logQueryForUser(username, sqlLog);

	        activeSession.beginTransaction();
	        
	        String sql = null;
	        if (zip_code != null) {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, tree_debris as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND ((l.zip_code = :zip_code) OR (:zip_code is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";	
	        }
	        else {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, tree_debris as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";
	        }

	        
	        if (verboseLevel>0) {
	            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
	        }

	        Iterable<BigInteger> result = null;
	        try {

	        	System.out.println(username);

	        	Query query = activeSession
	        			.createSQLQuery(sql);

	        	sql = query.getQueryString();
	        	if (zip_code != null) {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
							.setParameter("zip_code", zip_code)
                            .list();
	        	}
	        	else {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
                            .list();
	        	}
	        	

	        	if (verboseLevel>0) {
					System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
				}
	        	if (verboseLevel>1) {
	        		System.out.println("[CustomQuery] Output:");
	        		for(BigInteger r : result){
	       				System.out.print("\t");
	        			System.out.print(r);
	       				System.out.println("\t");
	        		}
	        	}
	        }
	        catch(Exception e) {
	        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
	        }

	        activeSession.getTransaction().commit();
	        return result;
	            }
	 
	 @SuppressWarnings("unchecked")
	    public Iterable<BigInteger> findLocationSpecificTreeTrims(
	    		String username,
	    		String street_address,
	            Integer zip_code
	            ) {
		 	
		 String sqlLog = null;
		 	if (zip_code != null) {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, tree_trims as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND ((l.zip_code = " + zip_code + " ) OR ( " + zip_code + " is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
		 	else {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, tree_trims as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
	        
	        
	        queryLogger.logQueryForUser(username, sqlLog);

	        activeSession.beginTransaction();
	        
	        String sql = null;
	        if (zip_code != null) {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, tree_trims as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND ((l.zip_code = :zip_code) OR (:zip_code is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";	
	        }
	        else {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, tree_trims as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";
	        }

	        
	        if (verboseLevel>0) {
	            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
	        }

	        Iterable<BigInteger> result = null;
	        try {

	        	System.out.println(username);

	        	Query query = activeSession
	        			.createSQLQuery(sql);

	        	sql = query.getQueryString();
	        	if (zip_code != null) {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
							.setParameter("zip_code", zip_code)
                            .list();
	        	}
	        	else {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
                            .list();
	        	}
	        	

	        	if (verboseLevel>0) {
					System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
				}
	        	if (verboseLevel>1) {
	        		System.out.println("[CustomQuery] Output:");
	        		for(BigInteger r : result){
	       				System.out.print("\t");
	        			System.out.print(r);
	       				System.out.println("\t");
	        		}
	        	}
	        }
	        catch(Exception e) {
	        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
	        }

	        activeSession.getTransaction().commit();
	        return result;
	            }
	 
	 @SuppressWarnings("unchecked")
	    public Iterable<BigInteger> findLocationSpecificAlleyLightsOut(
	    		String username,
	    		String street_address,
	            Integer zip_code
	            ) {
		 	
		 String sqlLog = null;
		 	if (zip_code != null) {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, alley_lights_out as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND ((l.zip_code = " + zip_code + " ) OR ( " + zip_code + " is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
		 	else {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, alley_lights_out as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
	        
	        
	        queryLogger.logQueryForUser(username, sqlLog);

	        activeSession.beginTransaction();
	        
	        String sql = null;
	        if (zip_code != null) {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, alley_lights_out as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND ((l.zip_code = :zip_code) OR (:zip_code is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";	
	        }
	        else {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, alley_lights_out as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";
	        }

	        
	        if (verboseLevel>0) {
	            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
	        }

	        Iterable<BigInteger> result = null;
	        try {

	        	System.out.println(username);

	        	Query query = activeSession
	        			.createSQLQuery(sql);

	        	sql = query.getQueryString();
	        	if (zip_code != null) {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
							.setParameter("zip_code", zip_code)
                            .list();
	        	}
	        	else {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
                            .list();
	        	}
	        	

	        	if (verboseLevel>0) {
					System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
				}
	        	if (verboseLevel>1) {
	        		System.out.println("[CustomQuery] Output:");
	        		for(BigInteger r : result){
	       				System.out.print("\t");
	        			System.out.print(r);
	       				System.out.println("\t");
	        		}
	        	}
	        }
	        catch(Exception e) {
	        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
	        }

	        activeSession.getTransaction().commit();
	        return result;
	            }
	 
	 @SuppressWarnings("unchecked")
	    public Iterable<BigInteger> findLocationSpecificStreetLightsAllOut(
	    		 	String username,
		    		String street_address,
		            Integer zip_code
		            ) {
			 	
			 String sqlLog = null;
			 	if (zip_code != null) {
			 		sqlLog = "select distinct a.id " + 
			        		"from requests as r, location as l, status as s, street_lights_all_out as a " + 
			        		"where (" + 
			        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
			        		"    AND ((l.zip_code = " + zip_code + " ) OR ( " + zip_code + " is null))" + 
			        		"    AND (l.locationId = s.locationId)" + 
			        		"    AND (s.statusId = a.statusId)" + 
			        		");" ;
			 	}
			 	else {
			 		sqlLog = "select distinct a.id " + 
			        		"from requests as r, location as l, status as s, street_lights_all_out as a " + 
			        		"where (" + 
			        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
			        		"    AND (l.locationId = s.locationId)" + 
			        		"    AND (s.statusId = a.statusId)" + 
			        		");" ;
			 	}
		        
		        
		        queryLogger.logQueryForUser(username, sqlLog);

		        activeSession.beginTransaction();
		        
		        String sql = null;
		        if (zip_code != null) {
		        	sql = "select distinct a.id " + 
			        		"from requests as r, location as l, status as s, street_lights_all_out as a " + 
			        		"where (" + 
			        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
			        		"    AND ((l.zip_code = :zip_code) OR (:zip_code is null))" + 
			        		"    AND (l.locationId = s.locationId)" + 
			        		"    AND (s.statusId = a.statusId)" + 
			        		");";	
		        }
		        else {
		        	sql = "select distinct a.id " + 
			        		"from requests as r, location as l, status as s, street_lights_all_out as a " + 
			        		"where (" + 
			        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
			        		"    AND (l.locationId = s.locationId)" + 
			        		"    AND (s.statusId = a.statusId)" + 
			        		");";
		        }

		        
		        if (verboseLevel>0) {
		            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
		        }

		        Iterable<BigInteger> result = null;
		        try {

		        	System.out.println(username);

		        	Query query = activeSession
		        			.createSQLQuery(sql);

		        	sql = query.getQueryString();
		        	if (zip_code != null) {
		        		result = (List<BigInteger>) query
								.setParameter("street_address", street_address)
								.setParameter("zip_code", zip_code)
	                            .list();
		        	}
		        	else {
		        		result = (List<BigInteger>) query
								.setParameter("street_address", street_address)
	                            .list();
		        	}
		        	

		        	if (verboseLevel>0) {
						System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
					}
		        	if (verboseLevel>1) {
		        		System.out.println("[CustomQuery] Output:");
		        		for(BigInteger r : result){
		       				System.out.print("\t");
		        			System.out.print(r);
		       				System.out.println("\t");
		        		}
		        	}
		        }
		        catch(Exception e) {
		        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
		        }

		        activeSession.getTransaction().commit();
		        return result;
	            }
	 
	 @SuppressWarnings("unchecked")
	    public Iterable<BigInteger> findLocationSpecificStreetLightsOneOut(
	    		String username,
	    		String street_address,
	            Integer zip_code
	            ) {
		 	
		 String sqlLog = null;
		 	if (zip_code != null) {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, street_lights_one_out as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND ((l.zip_code = " + zip_code + " ) OR ( " + zip_code + " is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
		 	else {
		 		sqlLog = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, street_lights_one_out as a " + 
		        		"where (" + 
		        		"    ((l.street_address = '" + street_address + "') OR ('" + street_address + "' is null) OR ('" + street_address + "' = '')) " + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");" ;
		 	}
	        
	        
	        queryLogger.logQueryForUser(username, sqlLog);

	        activeSession.beginTransaction();
	        
	        String sql = null;
	        if (zip_code != null) {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, street_lights_one_out as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND ((l.zip_code = :zip_code) OR (:zip_code is null))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";	
	        }
	        else {
	        	sql = "select distinct a.id " + 
		        		"from requests as r, location as l, status as s, street_lights_one_out as a " + 
		        		"where (" + 
		        		"    ((l.street_address = :street_address) OR (:street_address is null) OR (:street_address = ''))" + 
		        		"    AND (l.locationId = s.locationId)" + 
		        		"    AND (s.statusId = a.statusId)" + 
		        		");";
	        }

	        
	        if (verboseLevel>0) {
	            System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
	        }

	        Iterable<BigInteger> result = null;
	        try {

	        	System.out.println(username);

	        	Query query = activeSession
	        			.createSQLQuery(sql);

	        	sql = query.getQueryString();
	        	if (zip_code != null) {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
							.setParameter("zip_code", zip_code)
                            .list();
	        	}
	        	else {
	        		result = (List<BigInteger>) query
							.setParameter("street_address", street_address)
                            .list();
	        	}
	        	

	        	if (verboseLevel>0) {
					System.out.println("[CustomQuerys] Executing query:\n\t" + sql);
				}
	        	if (verboseLevel>1) {
	        		System.out.println("[CustomQuery] Output:");
	        		for(BigInteger r : result){
	       				System.out.print("\t");
	        			System.out.print(r);
	       				System.out.println("\t");
	        		}
	        	}
	        }
	        catch(Exception e) {
	        	System.err.println("Failed to excecute " + sql + ": " + e.getMessage());
	        }

	        activeSession.getTransaction().commit();
	        return result;
	            }
	 
}
