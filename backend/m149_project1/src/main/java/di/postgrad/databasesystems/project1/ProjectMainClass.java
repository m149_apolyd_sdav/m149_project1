package di.postgrad.databasesystems.project1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectMainClass{
    public static void main(String [] argv){
        SpringApplication.run(ProjectMainClass.class, argv);
    }
}