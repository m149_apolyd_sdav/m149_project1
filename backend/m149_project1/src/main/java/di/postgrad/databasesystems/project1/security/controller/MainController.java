package di.postgrad.databasesystems.project1.security.controller;

import java.security.Principal;

import di.postgrad.databasesystems.project1.domain.AppUser;
import di.postgrad.databasesystems.project1.security.EncryptedPasswordUtils;
import di.postgrad.databasesystems.project1.security.UserDetailsServiceImpl;
import di.postgrad.databasesystems.project1.security.WebUtils;
import di.postgrad.databasesystems.project1.service.AppUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

	@Autowired
	UserDetailsServiceImpl userDetailsService;
	@Autowired
	AppUserService appUserService;

	@GetMapping(value = { "/", "/welcome" }, consumes = MediaType.ALL_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	 ModelAndView welcomePage() {
		ModelAndView modelAndView = new ModelAndView("authentication/welcomePage");
		modelAndView.addObject("title", "Welcome");
		modelAndView.addObject("message", "Welcome to 311-Chicago Requests Web Site!");
		return modelAndView;
	}

	@GetMapping(value = "/login", consumes = MediaType.ALL_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	ModelAndView loginPage() throws Exception {
		ModelAndView modelAndView = new ModelAndView("authentication/loginPage");
		return modelAndView;
	}

	@GetMapping(value = "/register", consumes = MediaType.ALL_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	ModelAndView register() throws Exception {
		ModelAndView modelAndView = new ModelAndView("authentication/registerPage");
		return modelAndView;
	}
	
	@PostMapping(value = "registerCheck", consumes = MediaType.ALL_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	ModelAndView registerCheck(
			 String username,
			 String password,
			 String confirmPassword,
			 String address,
			 String email) throws Exception {
		AppUser app_user= userDetailsService.findAppUserByUsername(username);
		ModelAndView modelAndView = null;
		if (app_user!=null) {
			modelAndView = new ModelAndView("authentication/registerPage");
			modelAndView.addObject("message", "username_already_exists");
			return modelAndView;
		}
		else {
			if ((password.length() < 4) || (!password.equals(confirmPassword)) ) {
				modelAndView = new ModelAndView("authentication/registerPage");
				modelAndView.addObject("message", "wrong_password");
				return modelAndView;
			}
		}
		modelAndView = new ModelAndView("authentication/loginPage");
		String encrypted_password = EncryptedPasswordUtils.encryptePassword(password);
		userDetailsService.addAppUser(username, encrypted_password, address, email );
		modelAndView.addObject("message", "registration_succeeded");
		return modelAndView;
	}

	@GetMapping(value = "authentication/admin", consumes = MediaType.ALL_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	ModelAndView admin(Principal principal) throws Exception {
		ModelAndView modelAndView = new ModelAndView("authentication/adminPage");

		// After user login successfully.
		String userName = principal.getName();

		System.out.println("User Name: " + userName);

		User loginedUser = (User) ((Authentication) principal).getPrincipal();

		String userInfo = WebUtils.toString(loginedUser);
		String userInfoName = (loginedUser.getUsername());
		modelAndView.addObject("userInfoName", userInfoName);
		modelAndView.addObject("userInfo", userInfo);
		modelAndView.addObject("users", appUserService.getAppUsers());
		return modelAndView;
	}
	
	@GetMapping(value = "authentication/403", consumes = MediaType.ALL_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	ModelAndView accessDenied(Principal principal) throws Exception {
		ModelAndView modelAndView = new ModelAndView("authentication/403Page");

		if (principal != null) {
			User loginedUser = (User) ((Authentication) principal).getPrincipal();

			String userInfo = WebUtils.toString(loginedUser);

			modelAndView.addObject("userInfo", userInfo);

			String message = "Hi " + principal.getName() //
			+ "<br> You do not have permission to access this page!";
			modelAndView.addObject("message", message);

		}
		return modelAndView;
	}

}