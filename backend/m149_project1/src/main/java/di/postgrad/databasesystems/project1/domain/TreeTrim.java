package di.postgrad.databasesystems.project1.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the tree_trims database table.
 * 
 */
@Entity
@Table(name="tree_trims")
public class TreeTrim implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name="location_of_trees")
	private String locationOfTrees;

	//bi-directional many-to-one association to Status
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="statusid", nullable = false)
	private Status status;

	public TreeTrim() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocationOfTrees() {
		return this.locationOfTrees;
	}

	public void setLocationOfTrees(String locationOfTrees) {
		this.locationOfTrees = locationOfTrees;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}