package di.postgrad.databasesystems.project1.controller;

import di.postgrad.databasesystems.project1.domain.*;
import di.postgrad.databasesystems.project1.service.*;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
//@RequestMapping("/user/home")
public class RequestController {

    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private final String relativeEndpointHomePath = "user/";
    private final String relativeViewHomePath = "user/";

    @Autowired
    RequestService requestService;
    @Autowired
    StoredFunctionsService storedFunctionsService;
    @Autowired
    NewIncidentService newIncidentService;
    @Autowired
    AbandonedVehicleService abandonedVehicleService;
    @Autowired
    GarbageCartService garbageCartService;
    @Autowired
    GraffitiRemovalService graffitiRemovalService;
    @Autowired
    PotHolesReportedService potHolesReportedService;
    @Autowired
    RodentBaitingService rodentBaitingService;
    @Autowired
    SanitationCodeComplaintService sanitationCodeComplaintsService;
    @Autowired
    TreeDebriService treeDebriService;
    @Autowired
    TreeTrimService treeTrimService;
    @Autowired
    AlleyLightsOutService alleyLightsOutService;
    @Autowired
    StreetLightsAllOutService streetLightsAllOutService;
    @Autowired
    StreetLightsOneOutService streetLightsOneOutService;
   

    @GetMapping(value = "user/home", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView home() throws Exception {
        ModelAndView modelAndView = new ModelAndView("user/home");
        modelAndView.addObject("requests", requestService.getRequests());
        return modelAndView;
    }
    
    @GetMapping(value = relativeEndpointHomePath + "storedFunctions", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView storedFunctions() throws Exception {
        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "storedFunctions");
        modelAndView.addObject("requests", requestService.getRequests());
        return modelAndView;
    }
    
    @PostMapping(value = relativeEndpointHomePath + "function01", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView function01(@RequestParam String startingDate ,
                            @RequestParam String endingDate,
                            Principal principal) throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "storedFunctions/function01");
        modelAndView.addObject("function01Results", storedFunctionsService.getFunction01(df.parse(startingDate), df.parse(endingDate), principal == null ? null : principal.getName()));
        return modelAndView;
    }

    @PostMapping(value = relativeEndpointHomePath + "function02", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView function02(@RequestParam String typeOfServiceRequest,
                            @RequestParam String startingDate ,
                            @RequestParam String endingDate,
                            Principal principal) throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "storedFunctions/function02");
        modelAndView.addObject("function02Results", storedFunctionsService.getFunction02(typeOfServiceRequest, df.parse(startingDate), df.parse(endingDate), principal == null ? null : principal.getName()));
        return modelAndView;
    }

    @PostMapping(value = relativeEndpointHomePath + "function03", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView function03(@RequestParam String date,
            				Principal principal) throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "storedFunctions/function03");
        modelAndView.addObject("function03Results", storedFunctionsService.getFunction03(df.parse(date), principal == null ? null : principal.getName()));
        return modelAndView;
    }

    @PostMapping(value = relativeEndpointHomePath + "function04", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView function04(
            @RequestParam String startingDate,
            @RequestParam String endingDate,
            Principal principal) throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "storedFunctions/function04");
        modelAndView.addObject("function04Results", storedFunctionsService.getFunction04(df.parse(startingDate), df.parse(endingDate), principal == null ? null : principal.getName()));
        return modelAndView;
    }

    @PostMapping(value = relativeEndpointHomePath + "function05", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView function05(
            @RequestParam Float min_lon,
            @RequestParam Float max_lon,
            @RequestParam Float min_lat,
            @RequestParam Float max_lat,
            @RequestParam String creationDate,
            Principal principal) throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "storedFunctions/function05");
        modelAndView.addObject("function05Results", storedFunctionsService.getFunction05(min_lon, max_lon, min_lat, max_lat, df.parse(creationDate), principal == null ? null : principal.getName()));
        return modelAndView;
    }

    @PostMapping(value = relativeEndpointHomePath + "function06", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView function06(
            @RequestParam String startingDate,
            @RequestParam String endingDate,
            Principal principal )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "storedFunctions/function06");
        modelAndView.addObject("function06Results", storedFunctionsService.getFunction06(df.parse(startingDate), df.parse(endingDate), principal == null ? null : principal.getName()));
        return modelAndView;
    }

    @PostMapping(value = relativeEndpointHomePath + "function07", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView function07(
    		Principal principal)throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "storedFunctions/function07");
        modelAndView.addObject("function07Results", storedFunctionsService.getFunction07(principal == null ? null : principal.getName()));
        return modelAndView;
    }

    @PostMapping(value = relativeEndpointHomePath + "function08", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView function08(Principal principal)throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "storedFunctions/function08");
        modelAndView.addObject("function08Results", storedFunctionsService.getFunction08(principal == null ? null : principal.getName()));
        return modelAndView;
    }

    @PostMapping(value = relativeEndpointHomePath + "function09", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView function09(
            @RequestParam Float maxNumberOfPremisesBaited,
            Principal principal
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "storedFunctions/function09");
        modelAndView.addObject("function09Results", storedFunctionsService.getFunction09(maxNumberOfPremisesBaited, principal == null ? null : principal.getName()));
        return modelAndView;
    }

    @PostMapping(value = relativeEndpointHomePath + "function10", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView function10(
            @RequestParam Float maxNumberOfPremisesWithGarbage,
            Principal principal
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "storedFunctions/function10");
        modelAndView.addObject("function10Results", storedFunctionsService.getFunction10(maxNumberOfPremisesWithGarbage, principal == null ? null : principal.getName()));
        return modelAndView;
    }

    @PostMapping(value = relativeEndpointHomePath + "function11", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView function11(
            @RequestParam Float maxNumberOfPremisesWithRats,
            Principal principal
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "storedFunctions/function11");
        modelAndView.addObject("function11Results", storedFunctionsService.getFunction11(maxNumberOfPremisesWithRats, principal == null ? null : principal.getName()));
        return modelAndView;
    }

    @PostMapping(value = relativeEndpointHomePath + "function12", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView function12(
            @RequestParam Float numberOfPotholesFilledOnBlock,
            @RequestParam Float numberOfPremisesBaited,
            @RequestParam String completionDate,
            Principal principal
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "storedFunctions/function12");
        modelAndView.addObject("function12Results", storedFunctionsService.getFunction12(numberOfPotholesFilledOnBlock, numberOfPremisesBaited, df.parse(completionDate), principal == null ? null : principal.getName()));
        return modelAndView;
    }

    @GetMapping(value = relativeEndpointHomePath + "newIncident", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView newIncident(
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncident");
        return modelAndView;
    }

    @GetMapping(value = relativeEndpointHomePath + "newIncidents/newAbandonedVehicleComplaint", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView newAbandonedVehicleComplaint(
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newAbandonedVehicleComplaint");
        return modelAndView;
    }

    @GetMapping(value = relativeEndpointHomePath + "newIncidents/newAlleyLightsOut", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView newAlleyLightsOut(
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newAlleyLightsOut");
        return modelAndView;
    }

    @GetMapping(value = relativeEndpointHomePath + "newIncidents/newGarbageCarts", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView newGarbageCarts(
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newGarbageCarts");
        return modelAndView;
    }

    @GetMapping(value = relativeEndpointHomePath + "newIncidents/newGraffitiRemoval", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView newGraffitiRemoval(
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newGraffitiRemoval");
        return modelAndView;
    }

    @GetMapping(value = relativeEndpointHomePath + "newIncidents/newPotholesReported", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView newPotHolesReported(
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newPotholesReported");
        return modelAndView;
    }

    @GetMapping(value = relativeEndpointHomePath + "newIncidents/newRodentBaiting", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView newRodentBaiting(
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newRodentBaiting");
        return modelAndView;
    }

    @GetMapping(value = relativeEndpointHomePath + "newIncidents/newSanitationCodeComplaints", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView newSanitationCodeComplaints(
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newSanitationCodeComplaints");
        return modelAndView;
    }

    @GetMapping(value = relativeEndpointHomePath + "newIncidents/newStreetLightsAllOut", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView newStreetLightsAllOut(
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newStreetLightsAllOut");
        return modelAndView;
    }

    @GetMapping(value = relativeEndpointHomePath + "newIncidents/newStreetLightsOneOut", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView newStreetLightsOneOut(
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newStreetLightsOneOut");
        return modelAndView;
    }

    @GetMapping(value = relativeEndpointHomePath + "newIncidents/newTreeDebris", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView newTreeDebris(
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newTreeDebris");
        return modelAndView;
    }

    @GetMapping(value = relativeEndpointHomePath + "newIncidents/newTreeTrims", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView newTreeTrims(
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newTreeTrims");
        return modelAndView;
    }

    @PostMapping(value = relativeEndpointHomePath + "displayAbandonedVehicleComplaint", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView displayAbandonedVehicleComplaint(
            Principal principal,
			@RequestParam String creation_date,
			@RequestParam String status,
			@RequestParam String completion_date,
			@RequestParam String service_request_number,
			@RequestParam String type_of_service_request,
			@RequestParam String license_plate,
			@RequestParam String vehicle_make_model,
			@RequestParam String vehicle_color,
			@RequestParam String current_activity,
			@RequestParam String most_recent_action,
			@RequestParam Float how_many_days_has_the_vehicle_been_reported_as_parked,
			@RequestParam String street_address,
			@RequestParam Integer zip_code,
			@RequestParam Float x_coordinate,
			@RequestParam Float y_coordinate,
			@RequestParam Short ward,
			@RequestParam Short police_district,
			@RequestParam Short community_area,
			@RequestParam Short ssa,
			@RequestParam Float latitude,
			@RequestParam Float longitude,
			@RequestParam String location
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newAbandonedVehicleComplaint");
        newIncidentService.addAbvandonedVehicleComplaint(principal != null ? principal.getName() : null, df.parse(creation_date), status, df.parse(completion_date), service_request_number, type_of_service_request, license_plate, vehicle_make_model, vehicle_color, current_activity, most_recent_action, how_many_days_has_the_vehicle_been_reported_as_parked, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location);
        return modelAndView;
    }


    @PostMapping(value = relativeEndpointHomePath + "displayAlleyLightsOut", consumes = MediaType.ALL_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    ModelAndView displayAlleyLightsOut(
            Principal principal,
			@RequestParam String creation_date,
			@RequestParam String status,
			@RequestParam String completion_date,
			@RequestParam String service_request_number,
			@RequestParam String type_of_service_request,
			@RequestParam String street_address,
			@RequestParam Integer zip_code,
			@RequestParam Float x_coordinate,
			@RequestParam Float y_coordinate,
			@RequestParam Short ward,
			@RequestParam Short police_district,
			@RequestParam Short community_area,
			@RequestParam Float latitude,
			@RequestParam Float longitude,
			@RequestParam String location
            )throws Exception {

        ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newAlleyLightsOut");
        newIncidentService.addAlleyLightsOut(principal != null ? principal.getName() : null, df.parse(creation_date), status, df.parse(completion_date), service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location);
        return modelAndView;
    }

@PostMapping(value = relativeEndpointHomePath + "displayGarbageCarts", consumes = MediaType.ALL_VALUE)
@ResponseStatus(value = HttpStatus.OK)
ModelAndView displayGarbageCarts(
        Principal principal,
		@RequestParam String creation_date,
		@RequestParam String status,
		@RequestParam String completion_date,
		@RequestParam String service_request_number,
		@RequestParam String type_of_service_request,
		@RequestParam String current_activity,
		@RequestParam String most_recent_action,
		@RequestParam String street_address,
		@RequestParam Integer zip_code,
		@RequestParam Float x_coordinate,
		@RequestParam Float y_coordinate,
		@RequestParam Short ward,
		@RequestParam Short police_district,
		@RequestParam Short community_area,
		@RequestParam Short ssa,
		@RequestParam Float latitude,
		@RequestParam Float longitude,
		@RequestParam String location,
		@RequestParam Float number_of_black_carts_delivered
        )throws Exception {

    ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newGarbageCarts");
    newIncidentService.addGarbageCarts(principal != null ? principal.getName() : null, df.parse(creation_date), status, df.parse(completion_date), service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location, number_of_black_carts_delivered);
    return modelAndView;
}


@PostMapping(value = relativeEndpointHomePath + "displayGraffitiRemoval", consumes = MediaType.ALL_VALUE)
@ResponseStatus(value = HttpStatus.OK)
ModelAndView displayGraffitiRemoval(
        Principal principal,
		@RequestParam String creation_date,
		@RequestParam String status,
		@RequestParam String completion_date,
		@RequestParam String service_request_number,
		@RequestParam String type_of_service_request,
		@RequestParam String street_address,
		@RequestParam Integer zip_code,
		@RequestParam Float x_coordinate,
		@RequestParam Float y_coordinate,
		@RequestParam Short ward,
		@RequestParam Short police_district,
		@RequestParam Short community_area,
		@RequestParam Short ssa,
		@RequestParam Float latitude,
		@RequestParam Float longitude,
		@RequestParam String location,
		@RequestParam String what_type_of_surface_is_the_graffiti_on,
		@RequestParam String where_is_the_graffiti_located
        ) throws Exception {

    ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newGraffitiRemoval");
    newIncidentService.addGraffitiRemoval(principal != null ? principal.getName() : null, df.parse(creation_date), status, df.parse(completion_date), service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location, what_type_of_surface_is_the_graffiti_on, where_is_the_graffiti_located);
    return modelAndView;
}

@PostMapping(value = relativeEndpointHomePath + "displayPotholesReported", consumes = MediaType.ALL_VALUE)
@ResponseStatus(value = HttpStatus.OK)
ModelAndView displayPotholesReported(
        Principal principal,
		@RequestParam String creation_date,
		@RequestParam String status,
		@RequestParam String completion_date,
		@RequestParam String service_request_number,
		@RequestParam String type_of_service_request,
		@RequestParam String current_activity,
		@RequestParam String most_recent_action,
		@RequestParam String street_address,
		@RequestParam Integer zip_code,
		@RequestParam Float x_coordinate,
		@RequestParam Float y_coordinate,
		@RequestParam Short ward,
		@RequestParam Short police_district,
		@RequestParam Short community_area,
		@RequestParam Short ssa,
		@RequestParam Float latitude,
		@RequestParam Float longitude,
		@RequestParam String location,
		@RequestParam Float number_of_potholes_filled_on_block
        ) throws Exception {

    ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newPotholesReported");
    newIncidentService.addPotholesReported(principal != null ? principal.getName() : null, df.parse(creation_date), status, df.parse(completion_date), service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, ssa, latitude, longitude, location, number_of_potholes_filled_on_block);
    return modelAndView;
}

@PostMapping(value = relativeEndpointHomePath + "displayRodentBaiting", consumes = MediaType.ALL_VALUE)
@ResponseStatus(value = HttpStatus.OK)
ModelAndView displayRodentBaiting(
        Principal principal,
		@RequestParam String creation_date,
		@RequestParam String status,
		@RequestParam String completion_date,
		@RequestParam String service_request_number,
		@RequestParam String type_of_service_request,
		@RequestParam String current_activity,
		@RequestParam String most_recent_action,
		@RequestParam String street_address,
		@RequestParam Integer zip_code,
		@RequestParam Float x_coordinate,
		@RequestParam Float y_coordinate,
		@RequestParam Short ward,
		@RequestParam Short police_district,
		@RequestParam Short community_area,
		@RequestParam Float latitude,
		@RequestParam Float longitude,
		@RequestParam String location,
		@RequestParam Float number_of_premises_baited,
		@RequestParam Float number_of_premises_with_garbage,
		@RequestParam Float number_of_premises_with_rats
        )throws Exception {

    ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newRodentBaiting");
    newIncidentService.addRodentBaiting(principal != null ? principal.getName() : null, df.parse(creation_date), status, df.parse(completion_date), service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, number_of_premises_baited, number_of_premises_with_garbage, number_of_premises_with_rats);
    return modelAndView;
}

@PostMapping(value = relativeEndpointHomePath + "displaySanitationCodeComplaints", consumes = MediaType.ALL_VALUE)
@ResponseStatus(value = HttpStatus.OK)
ModelAndView displaySanitationCodeComplaints(
        Principal principal,
		@RequestParam String creation_date,
		@RequestParam String status,
		@RequestParam String completion_date,
		@RequestParam String service_request_number,
		@RequestParam String type_of_service_request,
		@RequestParam String street_address,
		@RequestParam Integer zip_code,
		@RequestParam Float x_coordinate,
		@RequestParam Float y_coordinate,
		@RequestParam Short ward,
		@RequestParam Short police_district,
		@RequestParam Short community_area,
		@RequestParam Float latitude,
		@RequestParam Float longitude,
		@RequestParam String location,
		@RequestParam String what_is_the_nature_of_this_code_violation
        )throws Exception {

    ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newSanitationCodeComplaints");
    newIncidentService.addSanitationCodeComplaints(principal != null ? principal.getName() : null, df.parse(creation_date), status, df.parse(completion_date), service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, what_is_the_nature_of_this_code_violation);
    return modelAndView;
}

@PostMapping(value = relativeEndpointHomePath + "displayStreetLightsAllOut", consumes = MediaType.ALL_VALUE)
@ResponseStatus(value = HttpStatus.OK)
ModelAndView displayStreetLightsAllOut(
        Principal principal,
		@RequestParam String creation_date,
		@RequestParam String status,
		@RequestParam String completion_date,
		@RequestParam String service_request_number,
		@RequestParam String type_of_service_request,
		@RequestParam String street_address,
		@RequestParam Integer zip_code,
		@RequestParam Float x_coordinate,
		@RequestParam Float y_coordinate,
		@RequestParam Short ward,
		@RequestParam Short police_district,
		@RequestParam Short community_area,
		@RequestParam Float latitude,
		@RequestParam Float longitude,
		@RequestParam String location
        )throws Exception {

    ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newStreetLightsAllOut");
    newIncidentService.addStreetLightsAllOut(principal != null ? principal.getName() : null, df.parse(creation_date), status, df.parse(completion_date), service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location);
    return modelAndView;
}

@PostMapping(value = relativeEndpointHomePath + "displayStreetLightsOneOut", consumes = MediaType.ALL_VALUE)
@ResponseStatus(value = HttpStatus.OK)
ModelAndView displayStreetLightsOneOut(
        Principal principal,
		@RequestParam String creation_date,
		@RequestParam String status,
		@RequestParam String completion_date,
		@RequestParam String service_request_number,
		@RequestParam String type_of_service_request,
		@RequestParam String street_address,
		@RequestParam Integer zip_code,
		@RequestParam Float x_coordinate,
		@RequestParam Float y_coordinate,
		@RequestParam Short ward,
		@RequestParam Short police_district,
		@RequestParam Short community_area,
		@RequestParam Float latitude,
		@RequestParam Float longitude,
		@RequestParam String location
        )throws Exception {

    ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newStreetLightsOneOut");
    newIncidentService.addStreetLightsOneOut(principal != null ? principal.getName() : null, df.parse(creation_date), status, df.parse(completion_date), service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location);
    return modelAndView;
}

@PostMapping(value = relativeEndpointHomePath + "displayTreeDebris", consumes = MediaType.ALL_VALUE)
@ResponseStatus(value = HttpStatus.OK)
ModelAndView displayTreeDebris(
        Principal principal,
		@RequestParam String creation_date,
		@RequestParam String status,
		@RequestParam String completion_date,
		@RequestParam String service_request_number,
		@RequestParam String type_of_service_request,
		@RequestParam String current_activity,
		@RequestParam String most_recent_action,
		@RequestParam String street_address,
		@RequestParam Integer zip_code,
		@RequestParam Float x_coordinate,
		@RequestParam Float y_coordinate,
		@RequestParam Short ward,
		@RequestParam Short police_district,
		@RequestParam Short community_area,
		@RequestParam Float latitude,
		@RequestParam Float longitude,
		@RequestParam String location,
		@RequestParam String if_yes_where_is_the_debris_located
        )throws Exception {

    ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newTreeDebris");
    newIncidentService.addTreeDebris(principal != null ? principal.getName() : null, df.parse(creation_date), status, df.parse(completion_date), service_request_number, type_of_service_request, current_activity, most_recent_action, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, if_yes_where_is_the_debris_located);
    return modelAndView;
}

@PostMapping(value = relativeEndpointHomePath + "displayTreeTrims", consumes = MediaType.ALL_VALUE)
@ResponseStatus(value = HttpStatus.OK)
ModelAndView displayTreeTrims(
        Principal principal,
		String creation_date,
		String status,
		String completion_date,
		String service_request_number,
		String type_of_service_request,
		String street_address,
		Integer zip_code,
		Float x_coordinate,
		Float y_coordinate,
		Short ward,
		Short police_district,
		Short community_area,
		Float latitude,
		Float longitude,
		String location,
		String location_of_trees
        )throws Exception {

    ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "newIncidents/newTreeTrims");
    newIncidentService.addTreeTrims(principal != null ? principal.getName() : null, df.parse(creation_date), status, df.parse(completion_date), service_request_number, type_of_service_request, street_address, zip_code, x_coordinate, y_coordinate, ward, police_district, community_area, latitude, longitude, location, location_of_trees);
    return modelAndView;
}

@GetMapping(value = relativeEndpointHomePath + "locationSpecificRequests", consumes = MediaType.ALL_VALUE)
@ResponseStatus(value = HttpStatus.OK)
ModelAndView findLocationSpecificRequests(
        )throws Exception {

    ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "locationSpecificRequests");
    return modelAndView;
}

@PostMapping(value = relativeEndpointHomePath + "displayLocationSpecificRequests", consumes = MediaType.ALL_VALUE)
@ResponseStatus(value = HttpStatus.OK)
ModelAndView displayLocationSpecificRequests(
        Principal principal,
		String street_address,
		Integer zip_code
        )throws Exception {

    ModelAndView modelAndView = new ModelAndView(relativeViewHomePath + "locationSpecificRequests/results");
    
    Iterable<AbandonedVehicle> abandoned_vehicles = abandonedVehicleService.findLocationSpecificAbandonedVehicles(principal != null ? principal.getName() : null, street_address, zip_code);
    modelAndView.addObject("abandoned_vehicles", abandoned_vehicles);
    
    Iterable<GarbageCart> garbage_carts = garbageCartService.findLocationSpecificGarbageCarts(principal != null ? principal.getName() : null, street_address, zip_code);
    modelAndView.addObject("garbage_carts", garbage_carts);
    
    Iterable<GraffitiRemoval> graffiti_removals = graffitiRemovalService.findLocationSpecificGraffitiRemoval(principal != null ? principal.getName() : null, street_address, zip_code);
    modelAndView.addObject("graffiti_removals", graffiti_removals);
    
    Iterable<PotHolesReported> potholes_reporteds = potHolesReportedService.findLocationSpecificPotHolesReported(principal != null ? principal.getName() : null, street_address, zip_code);
    modelAndView.addObject("potholes_reporteds", potholes_reporteds);
    
    Iterable<RodentBaiting> rodent_baitings = rodentBaitingService.findLocationSpecificRodentBaiting(principal != null ? principal.getName() : null, street_address, zip_code);
    modelAndView.addObject("rodent_baitings", rodent_baitings);
    
    Iterable<SanitationCodeComplaint> sanitation_code_complaints = sanitationCodeComplaintsService.findLocationSpecificSanitationCodeComplaints(principal != null ? principal.getName() : null, street_address, zip_code);
    modelAndView.addObject("sanitation_code_complaints", sanitation_code_complaints);
    
    Iterable<TreeDebri> tree_debris = treeDebriService.findLocationSpecificTreeDebris(principal != null ? principal.getName() : null, street_address, zip_code);
    modelAndView.addObject("tree_debris", tree_debris);
    
    Iterable<TreeTrim> tree_trims = treeTrimService.findLocationSpecificTreeTrims(principal != null ? principal.getName() : null, street_address, zip_code);
    modelAndView.addObject("tree_trims", tree_trims);
    
    Iterable<AlleyLightsOut> alley_lights_outs = alleyLightsOutService.findLocationSpecificAlleyLightsOut(principal != null ? principal.getName() : null, street_address, zip_code);
    modelAndView.addObject("alley_lights_outs", alley_lights_outs);
    
    Iterable<StreetLightsAllOut> street_lights_all_outs = streetLightsAllOutService.findLocationSpecificStreetLightsAllOut(principal != null ? principal.getName() : null, street_address, zip_code);
    modelAndView.addObject("street_lights_all_outs", street_lights_all_outs);
    
    Iterable<StreetLightsOneOut> street_lights_one_outs = streetLightsOneOutService.findLocationSpecificStreetLightsOneOut(principal != null ? principal.getName() : null, street_address, zip_code);
    modelAndView.addObject("street_lights_one_outs", street_lights_one_outs);
    
    return modelAndView;
}



}
