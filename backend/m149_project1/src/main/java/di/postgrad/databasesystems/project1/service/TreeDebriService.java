package di.postgrad.databasesystems.project1.service;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import di.postgrad.databasesystems.project1.domain.TreeDebri;
import di.postgrad.databasesystems.project1.repository.TreeDebriRepository;

@Service
public class TreeDebriService {

    @Autowired
    TreeDebriRepository treeDebriRepository;

    @Autowired
    LocationSpecificRequestsService locationSpecificRequestsService;

    public TreeDebri addTreeDebri(TreeDebri treeDebri) {
        treeDebriRepository.save(treeDebri);
        return treeDebri;
    }

    public Iterable<TreeDebri> getTreeDebris(){
        return treeDebriRepository.findAll();
    }

    public TreeDebri getTreeDebri(Long treeDebriId){
      return treeDebriRepository.findOne(treeDebriId);
  }
    public Iterable<TreeDebri> findLocationSpecificTreeDebris(String username, String street_address, Integer zip_code){

        Iterable<BigInteger> tree_debris_id = locationSpecificRequestsService.findLocationSpecificTreeDebris(username, street_address, zip_code);
        List<TreeDebri> tree_debri_list = new ArrayList<>();
        if (tree_debris_id != null) {
        	for (BigInteger id : tree_debris_id) {
            	tree_debri_list.add(treeDebriRepository.findOne(id.longValue()));
            }
        }
        
        return (Iterable<TreeDebri>) tree_debri_list;
    }
}
