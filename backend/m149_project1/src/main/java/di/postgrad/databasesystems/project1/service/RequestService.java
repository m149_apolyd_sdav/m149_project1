package di.postgrad.databasesystems.project1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import di.postgrad.databasesystems.project1.domain.Request;
import di.postgrad.databasesystems.project1.repository.RequestRepository;

@Service
public class RequestService {

    @Autowired
    RequestRepository requestRepository;

    public Request addRequest(Request request) {
        requestRepository.save(request);
        return request;
    }

    public Iterable<Request> getRequests(){
        return requestRepository.findAll();
    }

    public Request getRequest(Integer requestId){
      return requestRepository.findOne(requestId);
    }
}
    
