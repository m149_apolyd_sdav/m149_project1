package di.postgrad.databasesystems.project1.service;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import di.postgrad.databasesystems.project1.domain.StreetLightsAllOut;
import di.postgrad.databasesystems.project1.repository.StreetLightsAllOutRepository;

@Service
public class StreetLightsAllOutService {

    @Autowired
    StreetLightsAllOutRepository streetLightsAllOutRepository;
    
    @Autowired
    LocationSpecificRequestsService locationSpecificRequestsService;

    public StreetLightsAllOut addStreetLightsAllOut(StreetLightsAllOut streetLightsAllOut) {
        streetLightsAllOutRepository.save(streetLightsAllOut);
        return streetLightsAllOut;
    }

    public Iterable<StreetLightsAllOut> getStreetLightsAllOuts(){
        return streetLightsAllOutRepository.findAll();
    }

    public StreetLightsAllOut getStreetLightsAllOut(Long streetLightsAllOutId){
      return streetLightsAllOutRepository.findOne(streetLightsAllOutId);
  }
    
    public Iterable<StreetLightsAllOut> findLocationSpecificStreetLightsAllOut(String username, String street_address, Integer zip_code){
    	
        Iterable<BigInteger> street_lights_all_outs_id = locationSpecificRequestsService.findLocationSpecificStreetLightsAllOut(username, street_address, zip_code);
        List<StreetLightsAllOut> street_lights_all_out_list = new ArrayList<>();
        if (street_lights_all_outs_id != null) {
        	for (BigInteger id : street_lights_all_outs_id) {
            	street_lights_all_out_list.add(streetLightsAllOutRepository.findOne(id.longValue()));
            }
        }
        
        return (Iterable<StreetLightsAllOut>) street_lights_all_out_list;
    }
}