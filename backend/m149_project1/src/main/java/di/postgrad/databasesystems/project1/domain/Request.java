package di.postgrad.databasesystems.project1.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the requests database table.
 * 
 */
@Entity
@Table(name="requests")
public class Request implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer typeid;

	@Column(name="type_of_service_request")
	private String typeOfServiceRequest;

	//bi-directional many-to-one association to Status
	@OneToMany(fetch = FetchType.LAZY, mappedBy="request")
	private List<Status> statuses;

	public Request() {
	}

	public Integer getTypeid() {
		return this.typeid;
	}

	public void setTypeid(Integer typeid) {
		this.typeid = typeid;
	}

	public String getTypeOfServiceRequest() {
		return this.typeOfServiceRequest;
	}

	public void setTypeOfServiceRequest(String typeOfServiceRequest) {
		this.typeOfServiceRequest = typeOfServiceRequest;
	}

	public List<Status> getStatuses() {
		return this.statuses;
	}

	public void setStatuses(List<Status> statuses) {
		this.statuses = statuses;
	}

	public Status addStatus(Status status) {
		getStatuses().add(status);
		status.setRequest(this);

		return status;
	}

	public Status removeStatus(Status status) {
		getStatuses().remove(status);
		status.setRequest(null);

		return status;
	}

}