package di.postgrad.databasesystems.project1.domain;


import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the actions database table.
 * 
 */
@Entity
@Table(name="actions")
@NamedQuery(name="Action.findAll", query="SELECT a FROM Action a")
public class Action implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long actionsid;

	@Column(name="current_activity")
	private String currentActivity;

	@Column(name="most_recent_action")
	private String mostRecentAction;

	//bi-directional many-to-one association to AbandonedVehicle
	@OneToMany(fetch = FetchType.LAZY, mappedBy="action")
	private List<AbandonedVehicle> abandonedVehicles;

	//bi-directional many-to-one association to GarbageCart
	@OneToMany(fetch = FetchType.LAZY, mappedBy="action")
	private List<GarbageCart> garbageCarts;

	//bi-directional many-to-one association to PotHolesReported
	@OneToMany(fetch = FetchType.LAZY, mappedBy="action")
	private List<PotHolesReported> potHolesReporteds;

	//bi-directional many-to-one association to RodentBaiting
	@OneToMany(fetch = FetchType.LAZY, mappedBy="action")
	private List<RodentBaiting> rodentBaitings;

	//bi-directional many-to-one association to TreeDebri
	@OneToMany(fetch = FetchType.LAZY, mappedBy="action")
	private List<TreeDebri> treeDebris;

	public Action() {
	}

	public Long getActionsid() {
		return this.actionsid;
	}

	public void setActionsid(Long actionsid) {
		this.actionsid = actionsid;
	}

	public String getCurrentActivity() {
		return this.currentActivity;
	}

	public void setCurrentActivity(String currentActivity) {
		this.currentActivity = currentActivity;
	}

	public String getMostRecentAction() {
		return this.mostRecentAction;
	}

	public void setMostRecentAction(String mostRecentAction) {
		this.mostRecentAction = mostRecentAction;
	}

	public List<AbandonedVehicle> getAbandonedVehicles() {
		return this.abandonedVehicles;
	}

	public void setAbandonedVehicles(List<AbandonedVehicle> abandonedVehicles) {
		this.abandonedVehicles = abandonedVehicles;
	}

	public AbandonedVehicle addAbandonedVehicle(AbandonedVehicle abandonedVehicle) {
		getAbandonedVehicles().add(abandonedVehicle);
		abandonedVehicle.setAction(this);

		return abandonedVehicle;
	}

	public AbandonedVehicle removeAbandonedVehicle(AbandonedVehicle abandonedVehicle) {
		getAbandonedVehicles().remove(abandonedVehicle);
		abandonedVehicle.setAction(null);

		return abandonedVehicle;
	}

	public List<GarbageCart> getGarbageCarts() {
		return this.garbageCarts;
	}

	public void setGarbageCarts(List<GarbageCart> garbageCarts) {
		this.garbageCarts = garbageCarts;
	}

	public GarbageCart addGarbageCart(GarbageCart garbageCart) {
		getGarbageCarts().add(garbageCart);
		garbageCart.setAction(this);

		return garbageCart;
	}

	public GarbageCart removeGarbageCart(GarbageCart garbageCart) {
		getGarbageCarts().remove(garbageCart);
		garbageCart.setAction(null);

		return garbageCart;
	}

	public List<PotHolesReported> getPotHolesReporteds() {
		return this.potHolesReporteds;
	}

	public void setPotHolesReporteds(List<PotHolesReported> potHolesReporteds) {
		this.potHolesReporteds = potHolesReporteds;
	}

	public PotHolesReported addPotHolesReported(PotHolesReported potHolesReported) {
		getPotHolesReporteds().add(potHolesReported);
		potHolesReported.setAction(this);

		return potHolesReported;
	}

	public PotHolesReported removePotHolesReported(PotHolesReported potHolesReported) {
		getPotHolesReporteds().remove(potHolesReported);
		potHolesReported.setAction(null);

		return potHolesReported;
	}

	public List<RodentBaiting> getRodentBaitings() {
		return this.rodentBaitings;
	}

	public void setRodentBaitings(List<RodentBaiting> rodentBaitings) {
		this.rodentBaitings = rodentBaitings;
	}

	public RodentBaiting addRodentBaiting(RodentBaiting rodentBaiting) {
		getRodentBaitings().add(rodentBaiting);
		rodentBaiting.setAction(this);

		return rodentBaiting;
	}

	public RodentBaiting removeRodentBaiting(RodentBaiting rodentBaiting) {
		getRodentBaitings().remove(rodentBaiting);
		rodentBaiting.setAction(null);

		return rodentBaiting;
	}

	public List<TreeDebri> getTreeDebris() {
		return this.treeDebris;
	}

	public void setTreeDebris(List<TreeDebri> treeDebris) {
		this.treeDebris = treeDebris;
	}

	public TreeDebri addTreeDebri(TreeDebri treeDebri) {
		getTreeDebris().add(treeDebri);
		treeDebri.setAction(this);

		return treeDebri;
	}

	public TreeDebri removeTreeDebri(TreeDebri treeDebri) {
		getTreeDebris().remove(treeDebri);
		treeDebri.setAction(null);

		return treeDebri;
	}

}