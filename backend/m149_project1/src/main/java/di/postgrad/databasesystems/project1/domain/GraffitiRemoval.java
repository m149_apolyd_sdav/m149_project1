package di.postgrad.databasesystems.project1.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the graffiti_removal database table.
 * 
 */
@Entity
@Table(name="graffiti_removal")
public class GraffitiRemoval implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name="what_type_of_surface_is_the_graffiti_on")
	private String whatTypeOfSurfaceIsTheGraffitiOn;

	@Column(name="where_is_the_graffiti_located")
	private String whereIsTheGraffitiLocated;

	//bi-directional many-to-one association to Status
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="statusid", nullable = false)
	private Status status;

	public GraffitiRemoval() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWhatTypeOfSurfaceIsTheGraffitiOn() {
		return this.whatTypeOfSurfaceIsTheGraffitiOn;
	}

	public void setWhatTypeOfSurfaceIsTheGraffitiOn(String whatTypeOfSurfaceIsTheGraffitiOn) {
		this.whatTypeOfSurfaceIsTheGraffitiOn = whatTypeOfSurfaceIsTheGraffitiOn;
	}

	public String getWhereIsTheGraffitiLocated() {
		return this.whereIsTheGraffitiLocated;
	}

	public void setWhereIsTheGraffitiLocated(String whereIsTheGraffitiLocated) {
		this.whereIsTheGraffitiLocated = whereIsTheGraffitiLocated;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}